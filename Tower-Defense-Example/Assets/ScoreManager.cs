﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{

	public int lives = 20;
	public int money = 100;

	public TMP_Text moneyText;
	public TMP_Text livesText;

	public GameObject gameOver;

	public void loseLife(int l = 1)
	{
		lives -= l;
		if (lives <= 0)
		{
			GameOver();
		}

	}
	public void GameOver()
	{
		//TODO: Game over screen instead
		Debug.Log("Game Over");

		gameOver.SetActive(true);
	}
	public void RestartScene()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	private void Update()
	{
		//FIXME: NOT EVERYFRAME
		moneyText.text = "Money:" + money.ToString() + " $";
		livesText.text = "Lives:" + lives.ToString() + " <3";
	}
}
