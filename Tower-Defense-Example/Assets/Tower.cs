﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
	Transform turretTransform;

	public GameObject bulletPrefab;
	public Transform shootPos;
	public float range = 50f;

	public int cost = 5;

	public float fireCooldown = 0.5f;
	float fireCooldownLeft = 0;

	public float damage = 1f;
	public float radius = 5f;

    // Start is called before the first frame update
    void Start()
    {
		turretTransform = transform.Find("Top");
    }

    // Update is called once per frame
    void Update()
    {
		//TODO: OPTIMIZE THIS
		Enemy[] enemies = GameObject.FindObjectsOfType<Enemy>();

		Enemy nearestEnemy = null;
		float dist = Mathf.Infinity;

		foreach(Enemy e in enemies)
		{
			float d = Vector3.Distance(this.transform.position, e.transform.position);
			if(nearestEnemy == null || d < dist)
			{
				nearestEnemy = e;
				dist = d;
			}
		}

		if (nearestEnemy == null)
		{
			Debug.Log("No enemies?");
			return;
		}

		Vector3 dir = nearestEnemy.transform.position - this.transform.position;

		Quaternion lookRot = Quaternion.LookRotation( dir );

		turretTransform.rotation = Quaternion.Euler(0, lookRot.eulerAngles.y, 0);

		fireCooldownLeft -= Time.deltaTime;
		if (fireCooldownLeft <= 0 && dir.magnitude <= range)
		{
			fireCooldownLeft = fireCooldown;
			ShootAt(nearestEnemy);
		}
	}

	public void ShootAt(Enemy e)
	{
		//TODO: fire at the tip!
		GameObject bulletGO = (GameObject)Instantiate(bulletPrefab, shootPos.position, shootPos.rotation);

		Bullet b = bulletGO.GetComponent<Bullet>();
		b.target = e.transform;
		b.radius = radius;
		b.damage = damage;
	}

}
