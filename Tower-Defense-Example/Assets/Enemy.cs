﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
	GameObject pathGO;

	Transform targetPathNode;
	int pathNodeIndex = 0;

	public float speed = 5f;
	public float HP = 1f;
	public int moneyValue = 1;
    // Start is called before the first frame update
    void Start()
    {
		pathGO = GameObject.Find("Waypoints");

    }

	void GetNextPathNode()
	{
		if (pathNodeIndex < pathGO.transform.childCount)
		{
			targetPathNode = pathGO.transform.GetChild(pathNodeIndex);
			pathNodeIndex++;
		}
		else
		{
			targetPathNode = null;
			ReachedGoal();
		}

	}

    // Update is called once per frame
    void Update()
    {
		if (targetPathNode == null)
		{
			GetNextPathNode();
			if (targetPathNode == null)
			{
				//We've run out of path!
				ReachedGoal();
				return;
			}
		}
		Vector3 dir = targetPathNode.position - this.transform.localPosition;

		float distThisFrame = speed * Time.deltaTime;

		if (dir.magnitude <= distThisFrame)
		{
			//we've reached the node
			targetPathNode = null;
		}
		else
		{
			//TODO: Smooth Motion
			//move towards node
			transform.Translate(dir.normalized * distThisFrame, Space.World);
			Quaternion targetLookRotation = Quaternion.LookRotation(dir);
			this.transform.rotation = Quaternion.Lerp(this.transform.rotation, targetLookRotation, Time.deltaTime * 5);
		}
    }
	void ReachedGoal()
	{
		FindObjectOfType<ScoreManager>().loseLife();
		Destroy(gameObject);
	}
	public void TakeDamage(float damage)
	{
		HP -= damage;
		if (HP <= 0)
		{
			Die();
		}
	}
	public void Die()
	{
		//TODO: More safely
		GameObject.FindObjectOfType<ScoreManager>().money += moneyValue;
		Destroy(gameObject);
	}
}
