﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
	float spawnCD = 0.25f;
	float spawnCDleft = 5f;

	[System.Serializable]
	public class WaveComponent{

		public GameObject enemyPrefab;
		public int num;
		[System.NonSerialized]
		public int spawned = 0;
	}

	public WaveComponent[] waveComp;

	private void Update()
	{
		bool didSpawn = false;
		spawnCDleft -= Time.deltaTime;
		if (spawnCDleft < 0)
		{
			spawnCDleft = spawnCD;

			//GO through the wave comp until find something to spawn.
			foreach(WaveComponent wc in waveComp)
			{
				if (wc.spawned < wc.num)
				{
					// SPAWN IT!
					wc.spawned++;

					Instantiate(wc.enemyPrefab, this.transform.position, this.transform.rotation);

					didSpawn = true;
					break;
				}
			}
			if (didSpawn == false)
			{
				//wave must be complete
				//TODO: Instatiate next wave object
				if (transform.parent.childCount > 1)
				{
					transform.parent.GetChild(1).gameObject.SetActive(true);
				}
				else
				{
					// That was the last wave -- what do we want to do?
					// what if instead of DESTROYING wave objects,
					// we just made them inactive, and then when we run
					// out of waves, we restart at the firsst one, 
					// but double the hp
				}

				Destroy(gameObject);
			}
		}
	}


}
