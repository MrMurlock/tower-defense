﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
	public float speed = 15f;
	public Transform target;

	public float damage = 1f;
	public float radius = 0f;

	float decay = 0.5f;

    // Update is called once per frame
    void Update()
    {
		if (target == null)
		{
			//Our enemy went away
			transform.Translate(Vector3.forward * Time.deltaTime * speed);

			decay -= Time.deltaTime;
			if (decay <= 0)
			{
				Destroy(gameObject);
			}

			return;
		}
		Vector3 dir = target.position - this.transform.localPosition;

		float distThisFrame = speed * Time.deltaTime;

		if (dir.magnitude <= distThisFrame)
		{
			//we've reached the node
			DoBulletHit(target.gameObject);
		}
		else
		{
			//TODO: Smooth Motion
			//move towards node
			transform.Translate(dir.normalized * distThisFrame, Space.World);
			Quaternion targetLookRotation = Quaternion.LookRotation(dir);
			this.transform.rotation = Quaternion.Lerp(this.transform.rotation, targetLookRotation, Time.deltaTime * 5);
		}
	}
	void DoBulletHit(GameObject T)
	{
		//TODO: what if its an exploding bullet with an area of effect?
		if (radius == 0)
		{
			T.GetComponent<Enemy>().TakeDamage(damage);
		}
		else
		{
			Collider[] cols = Physics.OverlapSphere(transform.position, radius);
			foreach (Collider c in cols)
			{
				Enemy e = c.GetComponent<Enemy>();
				if (e != null)
				{
					//TODO: You COULD do a falloff of damage based on distance, but that's rare in TD games.
					//TODO: Spawn explody stuff to signify a explosion
					e.GetComponent<Enemy>().TakeDamage(damage);
				}
			}
		}
		Destroy(gameObject);
	}
}
